
def find2020(expense_values):
    for x in (x for x in expense_values if x != ""):
        for y in (y for y in expense_values if y not in ["", x]):
            for z in (z for z in expense_values if z not in ["", x, y]):
                #print(x,"+",y,"=",x+y)
                x = int(x)
                y = int(y)
                z = int(z)
                if (x + y + z == 2020):
                    return (x * y * z)
            

with open("input.txt", "r") as asset_file:
    expense_values = asset_file.read().split("\n")

print(find2020(expense_values))
