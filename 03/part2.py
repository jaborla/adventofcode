def answer(input_lines, tup):
    right, down = tup
    global grid
    global max_size
    grid = parse(input_lines)
    start_x = 0
    start_y = 0
    trees = 0
    going = True
    x,y = start_x,start_y
    while going:
        try:
            trees += move_to(x,y)
            #print("x is",x,"y is",y,"grid is",move_to(x,y),"trees is",trees,"moving...")
            x += right
            if (x >= max_size):
                x -= max_size
            y += down
        except IndexError:
            going = False
    print("Right:",right,"Down:",down,"Total trees:",trees)
    return trees

def move_to(x,y):
    global grid
    return grid[y][x]

def get_value_of(char):
    if char == '#':
        return 1
    if char == '.':
        return 0

def parse(lines):
    global max_size
    grid = []
    #print("lines:",lines)
    for x in range(len(lines)):
        #print("x=",x,"lines[x]=",lines[x])
        line = []
        max_size = len(lines[x])
        for y in range(len(lines[x])):
            #print("y=",y,"lines[x][y]=",lines[x][y])
            line.append(get_value_of(lines[x][y]))
        grid.append(line)
    return grid

with open("input.txt", "r") as input_file:
    input_lines = input_file.read().split("\n")
    input_lines = [x for x in input_lines if x]

    total = 1
    distance = [(1,1), (3,1), (5,1), (7,1), (1,2)]
    for tup in distance:
        total *= answer(input_lines, tup)
    print("Final total:",total)