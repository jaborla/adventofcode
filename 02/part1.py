def parse(inputstring):
    sides = inputstring.split(":")
    left = parseleft(sides[0])
    right = sides[1].strip()
    return [left[0], left[1], left[2], right]

def parseleft(leftstring):
    parts = leftstring.split()
    letter = parts[1]
    numbers = [x.strip() for x in parts[0].split("-")]
    return [numbers[0], numbers[1], letter]


def isvalid(parsed):
    min = int(parsed[0])
    max = int(parsed[1])
    letter = parsed[2]
    password = parsed[3]
    found = 0
    for i in password:
        if (i == letter):
            found += 1
    print("Min:",min,"Max:",max,"Letter:",letter,"Password:",password,"Found:",found,"IsValid:",(min <= found <= max))
    return (min <= found <= max)


with open("input.txt", "r") as input_file:
    input_lines = input_file.read().split("\n")
    input_lines = [x for x in input_lines if x]

total_valid = 0
for line in input_lines:
    parsed = parse(line)
    if isvalid(parsed):
        total_valid+=1

print("Valid",total_valid)

