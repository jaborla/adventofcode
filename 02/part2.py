def parse(inputstring):
    sides = inputstring.split(":")
    left = parseleft(sides[0])
    right = sides[1].strip()
    return [left[0], left[1], left[2], right]

def parseleft(leftstring):
    parts = leftstring.split()
    letter = parts[1]
    numbers = [x.strip() for x in parts[0].split("-")]
    return [numbers[0], numbers[1], letter]


def isvalid(parsed):
    pos1 = int(parsed[0])-1
    pos2 = int(parsed[1])-1
    letter = parsed[2]
    password = parsed[3]
    print("Pos1:",pos1,"Pos2:",pos2,"Letter:",letter,"Password:",password,"IsValid:",((password[pos1] == letter) ^ (password[pos2] == letter)))
    return ((password[pos1] == letter) ^ (password[pos2] == letter))


with open("input.txt", "r") as input_file:
    input_lines = input_file.read().split("\n")
    input_lines = [x for x in input_lines if x]

total_valid = 0
for line in input_lines:
    parsed = parse(line)
    if isvalid(parsed):
        total_valid+=1

print("Valid",total_valid)

