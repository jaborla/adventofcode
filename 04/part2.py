def answer(input_lines):
    #print(input_lines)
    required_fields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']
    eye_colors = ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']
    valid = 0
    for line in input_lines:
        passport = parse(line)
        print("Testing passport",passport)
        if validate(passport, required_fields, eye_colors):
            valid += 1
            print("Valid passport found:",passport)
        else:
            print("Invalid passport found:",passport)
    print("valid total:",valid)


def parse(line):
    line_words = line.replace("\n"," ").split()
    fields = {}
    for field in line_words:
        ex = field.split(":")
        fields[ex[0]] = ex[1]
    return fields

def validate(passport, required_fields, eye_colors):
    for field in required_fields:
        try:
            x = passport[field]
            print("Field",field,"Value",x)
            if field == 'byr':
                if not checkdate(x, 1920, 2002):
                    return False
            elif field == 'iyr':
                if not checkdate(x, 2010, 2020):
                    return False
            elif field == 'eyr':
                if not checkdate(x, 2020, 2030):
                    return False
            elif field == 'hgt':
                hgt = int(x.replace('in','').replace('cm',''))
                is_cm = (x.find('cm') > 0)
                is_in = (x.find('in') > 0)
                if (not is_cm and not is_in):
                    print("invalid height type",x)
                    return False
                if (is_cm and (hgt > 193 or hgt < 150)):
                    print("cm wrong.  Value",x,"not between 150 and 193")
                    return False
                if (is_in and (hgt > 76 or hgt < 59)):
                    print("in wrong.  Value",x,"not between 59 and 76")
                    return False
            elif field == 'hcl':
                if len(x) != 7 or x[0] != '#' or int(x[1:],16) < 0:
                    print("Invalid hair color",x)
                    return False
            elif field == 'ecl':
                eye_colors.index(x)
            elif field == 'pid':
                if len(x) != 9:
                    print("Passport length wrong",x)
                    return False
                int(x)
        except KeyError:
            print("KeyError")
            return False
        except ValueError:
            print("ValueError")
            return False
        except:
            print("Other")
    return True

def checkdate(year, min, max):
    if (len(year) != 4 or int(year) < min or int(year) > max):
        print("Invalid date",year,"not between",min,"and",max)
        return False
    else:
        return True

with open("input.txt", "r") as input_file:
    input_lines = input_file.read().split("\n\n")
    input_lines = [x for x in input_lines if x]

    answer(input_lines)

    """byr (Birth Year) - four digits; at least 1920 and at most 2002.
    iyr (Issue Year) - four digits; at least 2010 and at most 2020.
    eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
    hgt (Height) - a number followed by either cm or in:

        If cm, the number must be at least 150 and at most 193.
        If in, the number must be at least 59 and at most 76.

    hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
    ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
    pid (Passport ID) - a nine-digit number, including leading zeroes."""