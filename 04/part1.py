def answer(input_lines):
    #print(input_lines)
    required_fields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']
    valid = 0
    for line in input_lines:
        passport = parse(line)
        #print(passport)
        if validate(passport, required_fields):
            valid += 1
            print("Valid passport found")
        else:
            print("Invalid passport found")
    print("valid total:",valid)


def parse(line):
    line_words = line.replace("\n"," ").split()
    fields = {}
    for field in line_words:
        ex = field.split(":")
        fields[ex[0]] = ex[1]
    return fields

def validate(passport, required_fields):
    for field in required_fields:
        try:
            x = passport[field]
        except KeyError:
            return False
    return True


with open("input.txt", "r") as input_file:
    input_lines = input_file.read().split("\n\n")
    input_lines = [x for x in input_lines if x]

    answer(input_lines)