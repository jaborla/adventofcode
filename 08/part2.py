def answer(input_lines):
    operations_list = []
    for line in input_lines:
        operation = parse(line)
        operations_list.append(operation)
    print(operations_list)
    current_pointer, accumulator = (0,0)
    full_actions_performed = perform_all_actions_until_end_or_repeat(operations_list, current_pointer, accumulator)
    change_pointer = len(full_actions_performed)-1
    while change_pointer >= 0:
        current_action = operations_list[full_actions_performed[change_pointer]]
        print("Change pointer is",change_pointer,"and current action is",current_action)
        if current_action["operation"] == "acc":
            pass
        else:
            new_operations_list = operations_list
            if current_action["operation"] == "jmp":
                new_operations_list[full_actions_performed[change_pointer]]["operation"] = "nop"
            else:
                new_operations_list[full_actions_performed[change_pointer]]["operation"] = "jmp"
            print("New operations list is",new_operations_list)
            # It's much more efficient to keep the partially-completed list and just roll it back until the change, but this works.
            # It does make the start_pointer and start_accumulators redundant though
            perform_all_actions_until_end_or_repeat(new_operations_list, 0, 0)
        change_pointer -= 1
    print("Error.  Got to the beginning of the loop with no changes.")


def perform_all_actions_until_end_or_repeat(operations_list, start_pointer, start_accumulator):
    actions_performed = []
    current_pointer, accumulator = (start_pointer,start_accumulator)
    while current_pointer not in actions_performed:
        actions_performed.append(current_pointer)
        current_pointer, accumulator = perform(current_pointer, accumulator, operations_list[current_pointer])
        if current_pointer == len(operations_list):
            print("We've exited!  Current accumulator is:",accumulator)
            exit()
        if current_pointer < 0:
            current_pointer += len(operations_list)
    print("Infinite loop on step",current_pointer,"with accumulator of",accumulator)
    return actions_performed

def parse(line):
    our_array = line.split(" ")
    #print("array is:",our_array)
    op = {"operation": our_array[0], "number": our_array[1]}
    return op

def perform(pointer, accumulator, operation):
    print("Performing operation",operation,"on",pointer,"with accumulator",accumulator)
    if (operation["operation"] == "nop"):
        print("NOP found.  Incrementing pointer")
        pointer += 1
    elif (operation["operation"] == "acc"):
        print("ACC found.  Modifying accumulator by",int(operation["number"]))
        accumulator += int(operation["number"])
        pointer += 1
    elif (operation["operation"] == "jmp"):
        print("JMP found.  Jumping by",int(operation["number"]))
        pointer += int(operation["number"])
    return (pointer, accumulator)

with open("input.txt", "r") as input_file:
    input_lines = input_file.read().split("\n")
    input_lines = [x for x in input_lines if x]

    answer(input_lines)