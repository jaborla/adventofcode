def answer(input_lines):
    operations_list = []
    for line in input_lines:
        operation = parse(line)
        operations_list.append(operation)
    print(operations_list)
    actions_performed = []
    current_pointer, accumulator = (0,0)
    while current_pointer not in actions_performed:
        actions_performed.append(current_pointer)
        current_pointer, accumulator = perform(current_pointer, accumulator, operations_list[current_pointer])
        if current_pointer < 0:
            current_pointer += len(operations_list)
    print("List of preformed actions:",actions_performed)
    print("Accumulator is",accumulator)

def parse(line):
    our_array = line.split(" ")
    #print("array is:",our_array)
    op = {"operation": our_array[0], "number": our_array[1]}
    return op

def perform(pointer, accumulator, operation):
    print("Performing operation",operation,"on",pointer,"with accumulator",accumulator)
    if (operation["operation"] == "nop"):
        print("NOP found.  Incrementing pointer")
        pointer += 1
    elif (operation["operation"] == "acc"):
        print("ACC found.  Increasing accumulator by",int(operation["number"]))
        accumulator += int(operation["number"])
        pointer += 1
    elif (operation["operation"] == "jmp"):
        print("JMP found.  Jumping by",int(operation["number"]))
        pointer += int(operation["number"])
    return (pointer, accumulator)



with open("input.txt", "r") as input_file:
    input_lines = input_file.read().split("\n")
    input_lines = [x for x in input_lines if x]

    answer(input_lines)