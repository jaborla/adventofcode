def answer(input_lines):
    total = 0
    print("Number of groups is",len(input_lines))
    num_groups = 0
    for group in input_lines:
        num_groups += 1
        print("Group",num_groups)
        line_list = parse(group)
        value = len(line_list)
        total += value
        print("Group value",value,"and running total:",total)
    print(total)

def parse(group):
    members = group.split("\n")
    current_set = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"}
    for member in members:
        if len(member) == 0:
            continue
        print("current set is",current_set,"and member is",set(member))
        current_set.intersection_update(set(member))
    print("uniques",current_set, "length",len(current_set))
    return current_set

with open("input.txt", "r") as input_file:
    input_lines = input_file.read().split("\n\n")
    input_lines = [x for x in input_lines if x]

    answer(input_lines)