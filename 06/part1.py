def answer(input_lines):
    total = 0
    for line in input_lines:
        line_list = parse(line)
        value = len(line_list)
        total += value
        print("Group's value",value," and running total:",total)
    print(total)

def parse(line):
    line_words = line.replace("\n","")
    uniques = set(line_words)
    print("uniques",uniques)
    return uniques

with open("input.txt", "r") as input_file:
    input_lines = input_file.read().split("\n\n")
    input_lines = [x for x in input_lines if x]

    answer(input_lines)