def answer(input_lines):
    global full_tree
    start = 'shiny gold'
    full_set = {}
    full_tree = {}
    for line in input_lines:
        inwards = parse(line)
        full_set.update(inwards)
    print("full_set",full_set)
    print("full_tree",full_tree)
    total_bags = find_insides(full_set, start) - 1 #Subtract the one bag you start with
    print("Total bags:", total_bags)

def parse(line):
    global full_tree
    first_split = line.split(" bags contain ")
    outside = first_split[0]
    inside_list = first_split[1].replace(" bags","").replace(' bag','').replace('.','').split(", ")
    inside, total = {}, {}
    for bag in inside_list:
        name = bag[2:]
        qty = bag[0]
        if qty == 'n':
            qty = '0'
        inside[name]= int(qty)
        try:
            full_tree[name].append(outside)
        except KeyError:
            full_tree[name] = [outside]
    total[outside] = inside
    #inside = [{x[2:] : x[0]}] for x in inside_list}
    return total

def find_insides(tree, start):
    print('Currently checking ',start)
    try:
        current_bag_insides = tree[start]
    except KeyError:
        print('The current bag, "',start,'" has no bags inside.')
    print(start,'bag\'s current_bag_insides is',current_bag_insides)
    running_total = 1
    for new_bag in current_bag_insides.keys():
        if new_bag.strip() == 'other':
            print(start,'bag ignoring "other"')
            return 1
        print(start,'has child bag "',new_bag,'" which I will now check')
        added_bags = (find_insides(tree, new_bag) * current_bag_insides[new_bag])
        running_total += added_bags
        print('I am',start,'and from child bag "',new_bag,'" I have gotten back',added_bags,'new bags, leading to a running total of',running_total)    
    print('I am',start,'and inside I carry',running_total,'bags, including myself.')
    return running_total



with open("input.txt", "r") as input_file:
    input_lines = input_file.read().split("\n")
    input_lines = [x for x in input_lines if x]

    answer(input_lines)