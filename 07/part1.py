def answer(input_lines):
    global full_tree
    start = 'shiny gold'
    full_set = {}
    full_tree = {}
    for line in input_lines:
        inwards = parse(line)
        full_set.update(inwards)
    print("full_set",full_set)
    print("full_tree",full_tree)
    all_outsides = find_all_outsides(full_tree, start)
    for bag in all_outsides:
        print("Possible outside:",bag)
    print("Total possible outsides:",len(all_outsides))

def parse(line):
    global full_tree
    first_split = line.split(" bags contain ")
    outside = first_split[0]
    inside_list = first_split[1].replace(" bags","").replace(' bag','').replace('.','').split(", ")
    inside, total = {}, {}
    for bag in inside_list:
        name = bag[2:]
        qty = bag[0]
        inside[name]= qty
        try:
            full_tree[name].append(outside)
        except KeyError:
            full_tree[name] = [outside]
    total[outside] = inside
    #inside = [{x[2:] : x[0]}] for x in inside_list}
    return total

def find_all_outsides(full_tree, start):
    valid_outsides = set()
    current_set = set(full_tree[start])
    print("full_tree[start]:",full_tree[start],"and its set",set(full_tree[start]))
    while current_set.difference(valid_outsides) != set():
        print("Current_set",current_set)
        print("Valid_outsides",valid_outsides)
        valid_outsides = valid_outsides.union(current_set)
        print("New valid outsides",valid_outsides)
        new_current_set = set()
        for bag in current_set:
            try:
                print("bag is",bag,"and it is contained in",full_tree[bag])
                new_current_set = new_current_set.union(full_tree[bag])
            except KeyError:
                print("bag is",bag,"and it is outsermost.")
                continue
        current_set = new_current_set
    return valid_outsides

with open("input.txt", "r") as input_file:
    input_lines = input_file.read().split("\n")
    input_lines = [x for x in input_lines if x]

    answer(input_lines)