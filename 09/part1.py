def answer(input_lines):
    preamble_size = 25
    is_valid = True
    current_pointer = preamble_size
    while is_valid:
        list_of_numbers = input_lines[current_pointer-preamble_size:current_pointer]
        is_valid = validate(input_lines[current_pointer],list_of_numbers)
        current_pointer += 1
    print("Failed to validate at pointer",current_pointer,"with number value of",input_lines[current_pointer-1])
        
def validate(my_number, list_of_numbers):
    found = False
    print("Checking",my_number,"against list",list_of_numbers)
    for i in list_of_numbers:
        second_list = [x for x in list_of_numbers]
        second_list.remove(i)
        for j in second_list:
            if my_number == i+j:
                found = True
                print("Found sum,",i,"+",j,"=",my_number)
                break
            if found:
                break
    return found
    

with open("input.txt", "r") as input_file:
    input_lines = input_file.read().split("\n")
    input_lines = [int(x) for x in input_lines if x]

    answer(input_lines)