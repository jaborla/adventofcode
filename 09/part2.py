def answer(input_lines):
    preamble_size = 25
    is_valid = True
    current_pointer = preamble_size
    while is_valid:
        list_of_numbers = input_lines[current_pointer-preamble_size:current_pointer]
        is_valid = validate(input_lines[current_pointer],list_of_numbers)
        current_pointer += 1
    print("Failed to validate at pointer",current_pointer,"with number value of",input_lines[current_pointer-1])
    weakness_number = input_lines[current_pointer-1] #105950735
    #phase 2
    master_pointer = len(input_lines)-1
    found = False
    while not found:
        sum = 0
        list_of_reagants = []
        current_pointer = master_pointer
        while sum < weakness_number:
            if input_lines[current_pointer] == weakness_number:
                sum = weakness_number +1
                break
            list_of_reagants.append(input_lines[current_pointer])
            sum += input_lines[current_pointer]
            print("Testing sum",sum,"from list",list_of_reagants)
            current_pointer -= 1
        if sum == weakness_number:
            print("Found a set!")
            print(list_of_reagants)
            print("Sum of first and last are",(list_of_reagants[0]+list_of_reagants[len(list_of_reagants)-1]))
            print("Sum of largest and smallest are",(max(list_of_reagants)+min(list_of_reagants)))
            found = True
        else:
            print("Sum got too big.  Sum is",sum,"from list",list_of_reagants)
            master_pointer -= 1

        
def validate(my_number, list_of_numbers):
    found = False
    print("Checking",my_number,"against list",list_of_numbers)
    for i in list_of_numbers:
        print("list is",list_of_numbers)
        second_list = [x for x in list_of_numbers]
        second_list.remove(i)
        for j in second_list:
            if my_number == i+j:
                found = True
                print("Found sum,",i,"+",j,"=",my_number)
                break
    return found
    

with open("input.txt", "r") as input_file:
    input_lines = input_file.read().split("\n")
    input_lines = [int(x) for x in input_lines if x]

    answer(input_lines)