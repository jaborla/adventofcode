def answer(input_lines):
    maximum = 0
    for line in input_lines:
        parsed_row, parsed_column = parse(line)
        value = turn_to_code(parsed_row, parsed_column)
        maximum = max(value, maximum)
        print("Maximum value is",maximum)

def parse(line):
    least, most = 0, 127
    left, right = 0, 7
    for letter in line:
        if letter == 'B':
            least = (most + 1 - least) // 2 + least 
        elif letter == 'F':
            most = (most + 1 - least) // 2 + least - 1
        elif letter == 'R':
            left = (right + 1 - left) // 2 + left 
        elif letter == 'L':
            right = (right + 1 - left) // 2 + left - 1
        #print("letter",letter,"least",least,"most",most,"left",left,"right",right)
    if least == most and left == right:
        return least, left
        print("Line",line,"has row",least,"and column",left)
    else:
        print("Problem found")
        print("letter",letter,"least",least,"most",most,"left",left,"right",right)
        exit()

def turn_to_code(row, column):
    value = int(row) * 8 + int(column)
    print("Row",row,"and column",column,"give result",value)
    return value

with open("input.txt", "r") as input_file:
    input_lines = input_file.read().split("\n")
    input_lines = [x for x in input_lines if x]

    answer(input_lines)